/**
 * Created by fitki on 02.07.2019.
 */

let passVisibilityIcon = document.querySelectorAll('i.fas');
passVisibilityIcon.forEach(function (elem) {
    elem.addEventListener('click', function () {

        const userPassInput = this.previousElementSibling;
        if(userPassInput.getAttribute('type')==='password'){
            userPassInput.setAttribute('type','text');
            this.classList.remove('fa-eye');
            this.classList.add('fa-eye-slash');
        }else{
            userPassInput.setAttribute('type','password');
            this.classList.remove('fa-eye-slash');
            this.classList.add('fa-eye');
        }
    })
});
const submitBtn = document.querySelector('button.btn');
submitBtn.addEventListener('click',function (event) {
    event.preventDefault();
    const warningMsg = document.createElement('p'),
        passFirstAttempt = document.getElementById('pass-1attempt'),
        passSecondAttempt = document.getElementById('pass-2attempt');
    warningMsg.innerText="Нужно ввести одинаковые значения!";
    warningMsg.classList.add('warning-message');

    if(passFirstAttempt.value!==passSecondAttempt.value || !passFirstAttempt.value || !passSecondAttempt.value){
        if(document.querySelector(".warning-message")){
            document.querySelector(".warning-message").remove()
        }
        passSecondAttempt.nextElementSibling.after(warningMsg);
    }else{
        if(document.querySelector(".warning-message")){
            document.querySelector(".warning-message").remove()
        }
        alert('You are Welcome!');
        }
});